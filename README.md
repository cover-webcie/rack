# Rack

> _**Rack** — A framework, typically with rails, bars, hooks, or pegs, for holding or storing things._

Rack is a small and simple boilerplate/framework for PHP based web applications 
that are integrated with the login API of the Cover website. It aims to ease the 
creation of websites or microservices that only need a small number of forms, 
tables and views and to provide a familiar project structure among most of 
Cover's PHP projects. Additionally, anybody with basic OOP skills and a basic 
understanding of the web should be able to easily comprehend the framework and 
setup a website.

Rack implements the following features:

* Basic templates with template inheritance
* Form rendering and validation (server side)
* SQL injection safe database interactions
* User authentication through the Cover API

Future improvements:

* [CSRF](https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)) protection (this is a desired feature).
* Escaping values by default in templates.
* Broader coverage of HTML [form input types](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_<input>_types)
* …and more! Please share your suggestions via [the issue tracker](https://bitbucket.org/cover-webcie/rack/issues).

Rather than providing a fully-featured API, Rack aims to provide just enough 
functionality to cover the basic needs. Projects with more complicated 
requirements should extend, subclass and/or override Rack code where needed to 
add functionality.

## Documentation

Rack is documented on the [Cover wiki](https://wiki.svcover.nl/documentation/rack/). 
This documentation does aim to give a quick overview of the available 
functionality, rather than documenting every single detail about the code. The 
code base is and should be small enough to quickly comprehend, so one who 
whishes to implement more advanced features is advised to inspect the source 
code as well.
